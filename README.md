# PicoVideoEmbed

This plugin for [Pico CMS](https://picocms.org/) adds support for embedding responsive videos from peertube, vimeo and youtube. It supports different link formats, such as:

```
[video https://kolektiva.media/videos/embed/a5c6b8e1-6e9e-4a89-ace4-03b6cf97ce07]
[video https://kolektiva.media/w/mtiCL28GP9gzLC2FPKjs6v]
[video https://tube.mfraters.net/videos/watch/9a2b78db-ef05-4770-9196-8a1fcd243f43]
[video https://vimeo.com/566821117]
[video https://player.vimeo.com/video/566821117]
[video https://youtu.be/NkdUs2Dxv4s]
[video https://www.youtube.com/watch?v=NkdUs2Dxv4s]
[video https://www.youtube.com/embed/NkdUs2Dxv4s]
```

## TODO

- add configuration options (peertube IP notice, ...)
- add unit tests for URL parsing
- also support bare video files?

## Thanks

I would like to thank s4ad, who built the [spiritual predecessor](https://github.com/s4ad/PicoEmbed) of this plugin.
